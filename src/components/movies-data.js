const movies=[
        {   id:1,
            title:"Terminator",
            Genre : "Action",
            Stock: 6,
            Rate :2.5
        },
        {   id:2,
            title:"Die Hard",
            Genre : "Action",
            Stock: 5,
            Rate :2.5
        },
            
        {   id:3,
            title:"Get Out",
            Genre : "Thriller",
            Stock: 8,
            Rate :3.5
        },
            
        {   
            id:4,
            title:"Trip To Italy",
            Genre : "Comedy",
            Stock: 5,
            Rate :2.5
        },

        {   
            id:5,
            title:"Airplane",
            Genre : "Comedy",
            Stock: 5,
            Rate :2.5
        },

        {   
            id:6,
            title:"Shawshank Redemption",
            Genre : "Action",
            Stock: 5,
            Rate :8.5
        },

        {   
            id:7,
            title:"Bad Grandpa",
            Genre : "Action",
            Stock: 5,
            Rate :2.5
        },

        {   
            id:8,
            title:"Hangover",
            Genre : "Comedy",
            Stock: 5,
            Rate :2.5
        }
    ]

function getAllMovies() {
    return movies;
}

export default getAllMovies;