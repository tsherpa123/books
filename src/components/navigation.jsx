import React from 'react';
import Movies from './movies';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'

import {NavLink} from 'react-router-dom'
const Navigation = () => {
    return (   
<Navbar bg="light" expand="lg">

  <div className="navbar-nav">
   
      <NavLink className="nav-item nav-link" to="/movies">Vidly</NavLink>
      <NavLink className="nav-item nav-link" to="/movies">Movies</NavLink>
      <NavLink className="nav-item nav-link" to="/customers">Customers</NavLink>
      <NavLink className="nav-item nav-link" to="/rentals">Rentals</NavLink>
      <NavLink className="nav-item nav-link" to="/loginform">Login</NavLink>


      {/* <Link to="/movies">Movies</Link>
      <Link to="/common/movieDetails">Detials</Link>
      <Link to="/customers">Customers</Link>
      <Link to="/">Home</Link> */}
  
  </div>
</Navbar>
   );
};
 
export default Navigation; 