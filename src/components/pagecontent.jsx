import React, { Component } from 'react';

import Navigation from './navigation';
import Movies from './movies';
import Customer from './customers';
import Rentals from './rentals';
import NotFound from './notfound';
import MovieForm from './movieForm';
import LoginForm from './loginform';
import { Route, Switch , Redirect} from 'react-router-dom';


 
const Pagecontent = () => {
    return (
        <div>
            <Navigation/>
            <div className="contents">
            <Switch> 

                    {/* <Route path="/common/movieDetails/:id" render={(props)=><MovieDetails id="item_id" {...props}/>}/> */}
                    <Route path="/loginform" component={LoginForm}/>
                    <Route path="/movies/:id" component={MovieForm}/>
                    <Route path="/rentals" component={Rentals}/>
                    <Route path="/movies" render={(props)=> <Movies sortBy="newwest" {...props}/>}/>
                    <Route path="/customers" component={Customer}/>
                    <Route path="/notfound" component={NotFound}/>
                    <Route path="/" exact component={Movies}/>
                    <Redirect to="/notfound"/>
            </Switch>
           </div>
    </div>);
}
 

export default Pagecontent;