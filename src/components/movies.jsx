import React, { Component } from 'react';
//import movies from './components/movies-data.js'
//import getAllMovies from './movies-data.js'
import MoviesTable from './moviesTable';
import {getMovies,deleteMovie} from './../fakeMovieService.js'
import Pagination from './common/pagination';
import {paginate } from '../utils/paginate';
import ListGroup from './common/listGroup';
import {getGenres} from './../fakeGenreService.js';

import _ from 'lodash';

class Movies extends Component {
    state = { 
         genres : [],
         movies :[],
         pageSize : 4,
         currentPage : 1,
         sortColumn :{path:'title', order:'asc'}
    };

    componentDidMount(){
        const genres= [{_id:"",name: "All"},...getGenres()];
        this.setState({movies:getMovies(), genres})
    }
    
    handleDelete = movieIDToDelete =>{
        console.log("delete clicked");
       const movies = this.state.movies.filter(m =>m._id !== movieIDToDelete);
       this.setState({movies:movies});
       // if key and values are same then ... this.setState({movies}) works too

    };

    handleLike = (movie) =>{
       
        const movies = [...this.state.movies]
        const index = movies.indexOf(movie);
        movies[index]  = {...movies[index]}
        movies[index].liked = !movies[index].liked;
        this.setState({movies})
    }

    handlePageChange = page =>{
        this.setState({currentPage :page})
    
    };

    handleGenreSelect =(genre) =>{
   
        this.setState({selectedGenre :genre, currentPage:1});

    };

    handleSort =(sortColumn)=>{
        
        this.setState({sortColumn})
    }

    getPageData = () =>{
            
            const allMovies = this.state.movies;
            const pageSize  = this.state.pageSize
            const currentPage = this.state.currentPage;
            const selectedGenre = this.state.selectedGenre;
            const sortColumn = this.state.sortColumn;
    
            const filtered = selectedGenre && selectedGenre._id
            ? allMovies.filter(m=>m.genre._id ===selectedGenre._id)
            : allMovies;

            const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order])
            const movies = paginate(sorted , currentPage, pageSize);
            return {totalCount: filtered.length , data: movies}

    }

render() { 

    const pageSize  = this.state.pageSize
    const currentPage = this.state.currentPage;
    const sortColumn = this.state.sortColumn;
    const {length :count} = this.state.movies;
    if(count==0) return <p>There are no movies in the database </p>

    const {totalCount , data:movies} = this.getPageData();
    if(totalCount ==  0){
        return <p>There are no movies in the database.</p>
    }
    return (
                      <div className="row">
                        <div className="col-2">
                            <ListGroup items={this.state.genres} 
                                       onItemSelect={this.handleGenreSelect}
                                       selectedItem={this.state.selectedGenre}/>
                        </div>
                        <div className="col">
                        <p>Showing {totalCount} movies in the database.</p>
                       <MoviesTable 
                                movies ={movies} 
                                onLike={this.handleLike} 
                                onDelete ={this.handleDelete}
                                onSort ={this.handleSort}
                                sortColumn ={sortColumn}/>
                        <Pagination 
                                    itemsCount={totalCount}
                                    pageSize={pageSize} 
                                    currentPage = {currentPage}
                                    onPageChange ={this.handlePageChange}/>
                                  
                                  </div>
                        </div>

                       );
    }
}
export default Movies;