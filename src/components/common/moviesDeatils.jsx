import React, { Component } from 'react';
const MovieDetails = ({...props}) => {
    const { id } = props.match.params;
    return (<h1>This is movie details page {id} </h1>);
};
 
export default MovieDetails;