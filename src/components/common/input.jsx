import React, { Component } from 'react';
const Inputs = ({name ,label,value,error, onChange}) => {
    return (  
    <div className="form-group">
    <label htmlFor={name}>{label}</label>
    <input autoFocus 
        onChange ={onChange}
        value={value}
        id={name}
        type="text"
        name = {name}
        className="form-control"/>
        {error && <div className="alert alert-danger">{error}</div>}
    </div>
    
    );
}
 
export default Inputs;