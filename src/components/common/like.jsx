import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
class Like extends Component {

   
    render() { 
        return (<i 
            className={this.setCalss()} 
            aria-hidden="false"
            onClick ={this.props.onClick}
            style={{cursor:'pointer'}}
            ></i> );
    }

    setCalss(){
        let classToSet = "fa fa-heart"; 
        this.props.liked ? classToSet= classToSet : classToSet = classToSet+"-o"
        return classToSet;
    }
}
export default Like;