import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Movies from './components/movies'
import Pagination from './components/common/pagination'

class App extends Component {
  state = { 
    numOfElementsPerPage: 0
   }

  setNumOfElementsPerPage =(numOfElementsPerPage) =>{
    this.setState({numOfElementsPerPage})
  }
  render() { 
    return ( <div className="App">
    <Movies moviesToDisplay={this.state.numOfElementsPerPage}/>
    <Pagination parentCallback={this.setNumOfElementsPerPage}/>
     <h2>{this.state.numOfElementsPerPage}</h2>

    </div> );
  }
}
 
export default App;

